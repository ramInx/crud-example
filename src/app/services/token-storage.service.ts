import { Injectable, OnInit } from '@angular/core';
import { SocialAuthService, GoogleLoginProvider, SocialUser, FacebookLoginProvider } from 'angularx-social-login';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
    providedIn: 'root'
})
export class TokenStorageService implements OnInit {

    socialUser: SocialUser;
    isLoggedIn = false;
    constructor(public socialAuthService: SocialAuthService) { }

    ngOnInit(): void {
    }
    loginWithGoogle(): void {
        this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
            (user) => {
                this.socialUser = user;
                this.isLoggedIn = (user != null);
                this.saveUser(user);
                if(this.isLoggedIn) {
                    this.saveToken(user.authToken);
                    window.location.reload();
                }
           console.log(user);
        });
    }
    loginWithFacebook(): void {
        this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
            (user) => {
                this.socialUser = user;
                this.isLoggedIn = (user != null);
                this.saveUser(user);
                if(this.isLoggedIn) {
                    this.saveToken(user.authToken);
                    window.location.reload();
                }
           console.log(user);
        });
    }
    signOut(): void {
       // this.socialAuthService.signOut();
        window.sessionStorage.clear();
    }

    public saveToken(token: string, social?: any): void {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }

    public getToken(): string | null {
        return window.sessionStorage.getItem(TOKEN_KEY);
    }

    public saveUser(user: any): void {
        window.sessionStorage.removeItem(USER_KEY);
        window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    }

    public getUser(): any {
        const user = window.sessionStorage.getItem(USER_KEY);
        if (user) {
            return JSON.parse(user);
        }

        return {};
    }
}
