import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './product';
import { environment } from './../../environments/environment';

const baseURL = environment.BASE_URL;

@Injectable({
  providedIn: 'root'
})
export class ProductService {

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json',
		})
	}

  constructor(private httpClient: HttpClient) { }

  readAll(): Observable<any> {
    return this.httpClient.post(`${baseURL}getAllPosts`,{type: 'business_user'});
  }

  read(id: any): Observable<any> {
    return this.httpClient.post(`${baseURL}getPostDetail`,{post_id: id});
  }

  create(data: any): Observable<any> {
    return this.httpClient.post(baseURL+'storePost', data,this.httpOptions);
  }

  update(id:Number, data:any): Observable<any> {
	data['post_id'] = id;
    return this.httpClient.post(`${baseURL}updatePost`, data,this.httpOptions);
  }

  delete(id:Number): Observable<any> {
    return this.httpClient.post(`${baseURL}deletePost`,{post_id: id},this.httpOptions);
  }
  
}