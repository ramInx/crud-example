import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { TokenStorageService } from '../services/token-storage.service';
import { SocialAuthService, GoogleLoginProvider, SocialUser, FacebookLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    form: any = {
      username: null,
      password: null
    };
    isLoggedIn = false;
    isLoginFailed = false;
    errorMessage = '';
    roles: string[] = [];
    socialUser: SocialUser;
  
    constructor(private authService: AuthService, private tokenStorage: TokenStorageService,private socialAuthService: SocialAuthService) { }
  
    ngOnInit(): void {
      if (this.tokenStorage.getToken()) {
        this.isLoggedIn = true;
      }
    //   this.socialAuthService.authState.subscribe((user) => {
    //     this.socialUser = user;
    //     this.isLoggedIn = (user != null);
    //     this.tokenStorage.saveUser(user);
    //     if(this.isLoggedIn) {
    //         this.tokenStorage.saveToken(user.authToken);
    //         this.reloadPage();
    //     }
    //     console.log(this.socialUser);
    //   });
    }
  
    onSubmit(): void {
      const { username, password } = this.form;
  
      this.authService.login(username, password).subscribe(
        response => {
            if(response.status) {

                this.tokenStorage.saveToken(response.data.loginToken);
                this.tokenStorage.saveUser(response.data);
        
                this.isLoginFailed = false;
                this.isLoggedIn = true;
                //this.roles = this.tokenStorage.getUser().roles;
                this.reloadPage();
            } else {
                this.errorMessage = response.message;
                this.isLoginFailed = true;
            }
        },
        err => {
          this.errorMessage = err.error.message;
          this.isLoginFailed = true;
        }
      );
    }
    
    loginWithGoogle(): void {
        this.tokenStorage.loginWithGoogle();        
        // this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
        //     (user) => {
        //             this.socialUser = user;
        // this.isLoggedIn = (user != null);
        // this.tokenStorage.saveUser(user);
        // if(this.isLoggedIn) {
        //     this.tokenStorage.saveToken(user.authToken);
        //     this.reloadPage();
        // }
        //    console.log(user);
        // });
    }
    loginWithFacebook(): void {
        this.tokenStorage.loginWithFacebook();
    }
    
    reloadPage(): void {
      window.location.reload();
    }
    logOut(): void {
        this.socialAuthService.signOut().then().catch();
    }
  }