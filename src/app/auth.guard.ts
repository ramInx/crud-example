import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from './services/token-storage.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private tokenStorageService: TokenStorageService, private router: Router) {}
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot):  true|UrlTree   {
        let isLoggedIn =  !!this.tokenStorageService.getToken();
        if(isLoggedIn) {
            return true;
        } else {
            return this.router.parseUrl('/login');
        }
    }

}
