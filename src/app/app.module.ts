import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductCreateComponent } from './components/product-create/product-create.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { authInterceptorProviders } from './helpers/auth.interceptor';
import { DataTablesModule } from 'angular-datatables';
import { ModalComponent } from './components/modal/modal.component';

import { SocialLoginModule, SocialAuthServiceConfig, GoogleLoginProvider,FacebookLoginProvider } from 'angularx-social-login';


@NgModule({
    declarations: [
        AppComponent,
        ProductCreateComponent,
        ProductDetailsComponent,
        ProductListComponent,
        LoginComponent,
        RegisterComponent,
        ModalComponent
    ],
    imports: [
        FormsModule,
        HttpClientModule,
        BrowserModule,
        DataTablesModule,
        ReactiveFormsModule,
        AppRoutingModule,
        SocialLoginModule
    ],
    providers: [
        authInterceptorProviders,
        {
            provide: 'SocialAuthServiceConfig',
            useValue: {
              autoLogin: false,
              providers: [
                {
                  id: GoogleLoginProvider.PROVIDER_ID,
                  provider: new GoogleLoginProvider(
                    '71760151589-ffq4t1sp54ps395gj23s9q44met6a1h9.apps.googleusercontent.com'
                  )
                },
                {
                    id: FacebookLoginProvider.PROVIDER_ID,
                    provider: new FacebookLoginProvider(
                      '165515565501376'
                    )
                }
              ]
            } as SocialAuthServiceConfig,
        } 
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
