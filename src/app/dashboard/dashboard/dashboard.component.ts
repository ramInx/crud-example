import { AfterViewInit, Component, OnInit, Renderer2,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { environment } from 'src/environments/environment';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
    dtOptions: DataTables.Settings = {};

    constructor(private router: Router, private productService: ProductService) { }

    ngOnInit(): void {
        const self = this;
        this.dtOptions = {
            serverSide: true,
            pageLength: 3,
            ajax: { url: environment.BASE_URL + 'getAllPosts2', method: 'Post', },
            columns: [{
                title: 'ID',
                data: 'id'
            }, {
                title: 'Title',
                data: 'title'
            }, {
                title: 'Catrgory',
                data: 'category'
            }, {
                title: 'Status',
                data: 'status'
            }, {
                title: 'Action',
                orderable: false,
                render: function (data: any, type: any, full: any) {
                    return self.getButton(full);
                }
            }],
            rowCallback: (row: Node, data: any[] | Object, index: number) => {
                // Unbind first in order to avoid any duplicate handler

                const editEle = $('td', row).find('button.edit-post');
                if (editEle) {
                    editEle.unbind('click');
                    editEle.bind('click', () => {
                        console.log(data);
                        this.router.navigate(["/products/" + editEle.attr("data-id")]);
                    });
                }
                const deleteEle = $('td', row).find('button.btn-delete');
                if (deleteEle) {
                    let id = deleteEle.attr("delete-id");
                    console.log(id);
                    deleteEle.unbind('click');
                    deleteEle.bind('click', () => {
                        this.deletePost(id);
                    });
                }

                return row;
            }
        };
    }

    getButton(data: any): string {
        let button = `<button class="btn btn-sm btn-primary edit-post" type="button" data-id="${data.id}" style="margin-right: 5px;">Edit</button><button class="btn btn-sm btn-danger btn-delete" type="button"  delete-id="${data.id}">Delete</button>`;
        return button;

    }

    deletePost(id: any): void {
        this.productService.delete(id)
            .subscribe(
                response => {
                    console.log(response);
                    this.reload();
                },
                error => {
                    console.log(error);
                });
    }

    reload(): void {
        $('#posts-table').DataTable().ajax.reload();
    }
}
