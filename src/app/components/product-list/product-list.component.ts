import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/services/product';
import { ProductService } from 'src/app/services/product.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';


@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

    products: any;
    currentProduct = {
        id: 0,
        title: '',
        category: '',
        content: '',
    };
    currentIndex = -1;
    name = '';
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject<any>();

    constructor(private productService: ProductService, private router: Router, public modalService: NgbModal) { }

    ngOnInit(): void {
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 5,
            lengthMenu: [5, 10, 20],
            order: [1, 'desc']
            //processing: true
        };
        this.readProducts();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    readProducts(): void {
        this.productService.readAll()
            .subscribe(
                products => {
                    if (products.status) {
                        this.products = products.data;
                        this.dtTrigger.next();
                    }
                    console.log(products);
                },
                error => {
                    console.log(error);
                });
    }

    refresh(): void {
        this.readProducts();
        this.currentProduct = {
            id: 0,
            title: '',
            category: '',
            content: '',
        };
        this.currentIndex = -1;
    }

    setCurrentProduct(product: Product, index: number): void {
        this.currentProduct = product;
        this.currentIndex = index;
    }



    deleteProduct(id: number): void {
        this.productService.delete(id)
            .subscribe(
                response => {
                    console.log(response);
                    this.readProducts();
                    this.router.navigate(['/products']);
                },
                error => {
                    console.log(error);
                });
    }

    openModal() {
        //Here you define the name of your component
        this.modalService.open(ModalComponent);
        //This section is if you want to have any variable to initialize
        //compConst.componentInstance.weight = undefined;
    }



}