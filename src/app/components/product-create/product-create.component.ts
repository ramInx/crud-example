import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  product = {
    title: '',
    category: '',
    content: ''
  };
  submitted = false;
	postForm = new FormGroup({
	title: new FormControl('',[
		Validators.required]),
	category: new FormControl(''),
	content: new FormControl(''),
  });

  
  constructor(private productService: ProductService) { }
  
  ngOnInit(): void {
	
  }

  get title() { return this.postForm.get('title'); }


  createProduct(): void {
    const data = {
      title: this.product.title,
      category: this.product.category,
      content: this.product.content
    };

    this.productService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newProduct(): void {
    this.submitted = false;
    this.product = {
      title: '',
      category: '',
      content: '',
    };
  }

}