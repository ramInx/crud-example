import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/services/product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  currentProduct!: Product;
  message = '';

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
	this.currentProduct = {
		id: 0,
		title: '',
		category: '',
		content: '',
	};
    this.getProduct(this.route.snapshot.paramMap.get('id'));
  }

  getProduct(id:any): void {
    this.productService.read(id)
      .subscribe(
        product => {
			let data = {
				id: product.data.id,
				title: product.data.title,
				category: product.data.category,
				content: product.data.content
			}
         	// this.currentProduct = product.data;
          	this.currentProduct = data;
          	console.log(product);
        },
        error => {
          console.log(error);
        });
  }  

  updateProduct(): void {
    this.productService.update(this.currentProduct.id, this.currentProduct)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The product was updated!';
        },
        error => {
          console.log(error);
        });
  }

  deleteProduct(): void {
    this.productService.delete(this.currentProduct.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/products']);
        },
        error => {
          console.log(error);
        });
  }
}
